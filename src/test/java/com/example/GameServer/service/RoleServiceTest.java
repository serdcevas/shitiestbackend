package com.example.GameServer.service;

import com.example.GameServer.model.Role;
import com.example.GameServer.model.RoleName;
import com.example.GameServer.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class RoleServiceTest {
    @Autowired
    MockMvc mvc;

    @Autowired
    WebApplicationContext wac;

    @Autowired
    RoleService rolService;

    @Test
    public void findByName() {
        for(RoleName role: RoleName.values()){
            Role userRole = rolService.findByName(role);
            assertEquals(role, userRole.getName());
        }
    }
}