package com.example.GameServer.service;

import com.example.GameServer.model.Role;
import com.example.GameServer.model.RoleName;
import com.example.GameServer.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class UserServiceTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    WebApplicationContext wac;

    @Autowired
    UserService userService;

    @Autowired
    RoleService rolService;

    @Test
    public void finByCode() {
        User user = new User("vardas","vardas");
        Role userRole = rolService.findByName(RoleName.ROLE_USER);
        user.setRoles(Collections.singleton(userRole));
        userService.save(user);
        User user2 = userService.finByCode(user.getUser_id());
        assertEquals(user.getUser_id(), user2.getUser_id());
    }

    @Test
    public void findByName() {
        User user = new User("vardas","vardas");
        Role userRole = rolService.findByName(RoleName.ROLE_USER);
        user.setRoles(Collections.singleton(userRole));
        userService.save(user);
        User user2 = userService.findByName(user.getName());
        assertEquals(user.getName(), user2.getName());
    }

    @Test
    public void save() {
        User user = new User("vardas","vardas");
        Role userRole = rolService.findByName(RoleName.ROLE_USER);
        user.setRoles(Collections.singleton(userRole));
        userService.save(user);
        assertEquals("vardas", userService.finByCode(user.getUser_id()).getName());
    }

    @Test
    public void getUsers() {
        User user = new User("vardas","vardas");
        Role userRole = rolService.findByName(RoleName.ROLE_USER);
        user.setRoles(Collections.singleton(userRole));
        userService.save(user);
        List<User> users = userService.getUsers();
        User lastUser=users.get(users.size()-1);
        assertEquals("vardas", lastUser.getName());
    }
}