package com.example.GameServer.service;

import com.example.GameServer.model.ChatMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class ChatServiceTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    WebApplicationContext wac;

    @Autowired
    ChatService chatService;

    @Test
    public void getMessages() {

    }

    @Test
    public void getMessages1() {

    }

    @Test
    public void saveChatMessage() {
        ChatMessage message= new ChatMessage();
        message.setMessage("message");
        chatService.saveGameMessage(message);
        List<ChatMessage> messages=chatService.getMessages(1);
        ChatMessage message2=  messages.get(messages.size()-1);
        assertEquals(message.getMessage_id(), message2.getMessage_id());
    }

    @Test
    public void saveGameMessage() {
        ChatMessage message= new ChatMessage();
        message.setMessage("message");
        message.setGame_id(1);
        chatService.saveGameMessage(message);
        List<ChatMessage> messages=chatService.getMessages(1);
        ChatMessage message2=  messages.get(messages.size()-1);
        assertEquals(message.getMessage_id(), message2.getMessage_id());
    }
}