package com.example.GameServer.service;

import com.example.GameServer.mapper.ChatMapper;
import com.example.GameServer.model.ChatMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ChatService {

    private ChatMapper chatMapper;

    @Autowired
    public ChatService(ChatMapper chatMapper){
        this.chatMapper=chatMapper;
    }

    public List<ChatMessage> getMessages(){
        return chatMapper.getMessages();
    }

    public List<ChatMessage> getMessages(Integer game_id){
        return chatMapper.getGameMessages(game_id);
    }

    @Transactional
    public ChatMessage saveChatMessage(ChatMessage message){
        chatMapper.insertMessage(message);
        return message;
    }
    @Transactional
    public ChatMessage saveGameMessage(ChatMessage message){
        chatMapper.insertGameMessage(message);
        return message;
    }
}
