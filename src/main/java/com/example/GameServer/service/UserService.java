package com.example.GameServer.service;

import com.example.GameServer.mapper.UserMapper;
import com.example.GameServer.model.Role;
import com.example.GameServer.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {

    private final UserMapper userMapper;

    @Autowired
    public UserService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public User finByCode(Long user_id)
    {
        return userMapper.finByCode(user_id);
    }

    public User findByName(String name){
        return userMapper.findByName(name);
    }
    @Transactional
    public User save(User user){
        userMapper.insertUser(user);
        saveUserRole(user);
        return user;
    }
    private void saveUserRole(User user){
        for(Role role: user.getRoles()){
            userMapper.insertUserRole(user.getUser_id(),role.getId());
        }
    }
    public List<User> getUsers(){
        return userMapper.getUsers();
    }
}
