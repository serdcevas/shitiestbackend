package com.example.GameServer.service;

import com.example.GameServer.model.ChatMessage;
import com.example.GameServer.model.Role;
import com.example.GameServer.model.RoleName;
import com.example.GameServer.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Facade {

    private final
    RoleService roleService;

    private final
    UserService userService;

    private final
    ChatService chatService;

    @Autowired
    public Facade(ChatService chatService, RoleService roleService, UserService userService) {
        this.chatService = chatService;
        this.roleService = roleService;
        this.userService = userService;

    }

    public Role findRoleByName(RoleName name){
        return roleService.findByName(name);
    }

    public User finByCode(Long user_id)
    {
        return userService.finByCode(user_id);
    }

    public User findByName(String name){
        return userService.findByName(name);
    }

    public User save(User user){
        userService.save(user);
        return user;
    }

    public boolean existsByUsername(String name){
        User user= findByName(name);
        return user != null;
    }

    public List<User> getUsers(){
        return userService.getUsers();
    }

    public List<ChatMessage> getMessages(){
        return chatService.getMessages();
    }

    public List<ChatMessage> getMessages(Integer game_id){
        return chatService.getMessages(game_id);
    }

    public ChatMessage saveChatMessage(ChatMessage message){
        chatService.saveChatMessage(message);
        return message;
    }
    public ChatMessage saveGameMessage(ChatMessage message){
        chatService.saveGameMessage(message);
        return message;
    }
}
