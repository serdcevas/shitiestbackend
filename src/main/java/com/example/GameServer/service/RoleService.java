package com.example.GameServer.service;

import com.example.GameServer.mapper.RoleMapper;
import com.example.GameServer.model.Role;
import com.example.GameServer.model.RoleName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {

    private RoleMapper roleMapper;

    @Autowired
    public RoleService(RoleMapper roleMapper){ this.roleMapper=roleMapper;}
    @Transactional
    public Role findByName(RoleName name){

        return roleMapper.findByName(name);
    }
}
