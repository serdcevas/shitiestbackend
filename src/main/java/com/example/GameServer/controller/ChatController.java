package com.example.GameServer.controller;

import com.example.GameServer.model.ChatMessage;
import com.example.GameServer.security.CurrentUser;
import com.example.GameServer.security.UserPrincipal;
import com.example.GameServer.service.ChatService;
import com.example.GameServer.service.Facade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value="api/chat")
public class ChatController {

    Logger logger = LoggerFactory.getLogger(ChatController.class);

    private final Facade facade;

    @Autowired
    public ChatController(Facade facade) {
        this.facade = facade;
    }

    @GetMapping("/{game_id}")
    public ResponseEntity<List<ChatMessage>> getGameChat(@PathVariable("game_id" ) Integer game_id, @CurrentUser UserPrincipal currentUser){
        List<ChatMessage> messages=facade.getMessages(game_id);
        if(messages.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(messages,HttpStatus.OK);
    }
    @GetMapping
    public ResponseEntity<List<ChatMessage>> getChat(){
        List<ChatMessage> messages=facade.getMessages();
        if(messages.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(messages,HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ChatMessage> createChatMessage(@Valid @RequestBody ChatMessage message){
        logger.debug("createChatMessage: {}", message);
        ChatMessage createdMessage = facade.saveChatMessage(message);
        return new ResponseEntity<>(createdMessage, HttpStatus.OK);
    }

    @PostMapping("/{game_id}")
    public ResponseEntity<ChatMessage> createGameMessage(@PathVariable("game_id" ) Integer game_id, @Valid @RequestBody ChatMessage message){
        logger.debug("createGameMessage: {}", message);
        message.setGame_id(game_id);
        ChatMessage createdMessage = facade.saveGameMessage(message);
        return new ResponseEntity<>(createdMessage, HttpStatus.OK);
    }
}
