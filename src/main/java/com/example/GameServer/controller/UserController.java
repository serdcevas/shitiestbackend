package com.example.GameServer.controller;

import com.example.GameServer.model.User;
import com.example.GameServer.service.Facade;
import com.example.GameServer.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="api/user")
public class UserController {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userservice;

    @GetMapping("/{user_id}")
    public ResponseEntity<User> getQuizByCode(@PathVariable Long user_id) {
        logger.debug("get User with id: {}", user_id);

        User user = userservice.finByCode(user_id);
        logger.debug("User found: {}", user);

        if (user != null) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping
    public ResponseEntity<List<User>> getUsers(){
        logger.debug("get Users: {}");

        List<User> users = userservice.getUsers();
        logger.debug("Users found: {}", users);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }
}
