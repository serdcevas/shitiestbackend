package com.example.GameServer.model;

public class ChatMessage {
    private Integer message_id;
    private String message;
    private Integer game_id;

    public ChatMessage(Integer message_id, String message) {
        this.message_id = message_id;
        this.message = message;
    }

    public ChatMessage() {
    }

    public void setMessage_id(Integer message_id) {
        this.message_id = message_id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMessage_id() {
        return message_id;
    }

    public String getMessage() {
        return message;
    }

    public Integer getGame_id() {
        return game_id;
    }

    public void setGame_id(Integer game_id) {
        this.game_id = game_id;
    }
}
