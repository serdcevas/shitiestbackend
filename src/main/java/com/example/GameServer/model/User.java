package com.example.GameServer.model;


import java.util.HashSet;
import java.util.Set;


public class User {

    private Long user_id;
    private String name;
    private String password;

    private Set<Role> roles = new HashSet<>();
    public User() {
    }

    public User(Long user_id, String name, String password) {
        this.user_id = user_id;
        this.name = name;
        this.password = password;
    }
    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
