package com.example.GameServer.mapper;


import com.example.GameServer.model.Role;
import com.example.GameServer.model.RoleName;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
@Mapper
public interface RoleMapper {

    @Select("SELECT * FROM \"role\" WHERE name=#{name}")
    @Results(value={
            @Result(property = "role_id",column = "role_id"),
            @Result(property = "name",column = "name")
    }
    )
    Role findByName(@Param("name") RoleName name);
}
