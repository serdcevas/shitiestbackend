package com.example.GameServer.mapper;

import com.example.GameServer.model.ChatMessage;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ChatMapper {

    @Select("SELECT * FROM \"chat_message\"")
    @Results(value={
            @Result(property = "message_id",column = "message_id"),
            @Result(property = "message",column = "message")
    }
    )
    List<ChatMessage> getMessages();

    @Select("SELECT * FROM \"chat_message\" WHERE game_id=#{game_id}")
    @Results(value={
            @Result(property = "message_id",column = "message_id"),
            @Result(property = "message",column = "message")
    }
    )
    List<ChatMessage> getGameMessages(Integer game_id);

    @Options(useGeneratedKeys = true, keyProperty = "message.message_id")
    @Insert("INSERT INTO \"chat_message\"(message) VALUES (#{message.message})")
    void insertMessage(@Param("message") ChatMessage message);

    @Options(useGeneratedKeys = true, keyProperty = "message.message_id")
    @Insert("INSERT INTO \"chat_message\"(message, game_id) VALUES (#{message.message}, #{message.game_id})")
    void insertGameMessage(@Param("message") ChatMessage message);
}
