package com.example.GameServer.mapper;

import com.example.GameServer.model.Role;
import com.example.GameServer.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
@Mapper
public interface UserMapper {
    @Select("SELECT * FROM \"user\" WHERE user_id=#{user_id}")
    @Results(value={
            @Result(property = "user_id",column = "user_id"),
            @Result(property = "name",column = "name"),
            @Result(property = "password",column = "password"),
            @Result(property = "roles",javaType = Set.class,column="user_id",
                    many=@Many(select="findRoles"))
    }
    )
    User finByCode(@Param("user_id") Long user_id);

    @Select("SELECT \"role\".name, \"role\".role_id FROM \"role\" " +
            "INNER JOIN user_roles on user_roles.role_id= \"role\".role_id " +
            "WHERE user_roles.user_id=#{user_id} ")
    @Results(value={
            @Result(property = "role_id",column = "role_id"),
            @Result(property = "name",column = "name"),
    })
    Set<Role>findRoles(Long user_id);

    @Select("SELECT * FROM \"user\" WHERE name=#{name}")
    @Results(value={
            @Result(property = "user_id",column = "user_id"),
            @Result(property = "name",column = "name"),
            @Result(property = "password",column = "password"),
            @Result(property = "roles",javaType = Set.class,column="user_id",
                    many=@Many(select="findRoles"))
    }
    )
    User findByName(@Param("name") String name);

    @Options(useGeneratedKeys = true, keyProperty = "user.user_id")
    @Insert("INSERT INTO \"user\"(name, password) VALUES (#{user.name}, #{user.password})")
    void insertUser(@Param("user") User user);

    @Insert("INSERT INTO \"user_roles\"(user_id, role_id) VALUES (#{user_id}, #{role_id})")
    void insertUserRole(@Param("user_id") Long user_id, @Param("role_id")Integer role_id);

    @Select("SELECT * FROM \"user\"")
    @Results(value={
            @Result(property = "user_id",column = "user_id"),
            @Result(property = "name",column = "name"),
            @Result(property = "password",column = "password"),
            @Result(property = "roles",javaType = Set.class,column="user_id",
                    many=@Many(select="findRoles"))
    }
    )
    List<User> getUsers();
}
