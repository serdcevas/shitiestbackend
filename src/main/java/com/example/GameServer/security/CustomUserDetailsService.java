package com.example.GameServer.security;

import com.example.GameServer.model.User;
import com.example.GameServer.service.Facade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final
    Facade facade;

    @Autowired
    public CustomUserDetailsService(Facade facade) {
        this.facade = facade;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user=facade.findByName(name);
        if(user != null) return UserPrincipal.create(user);
        throw new UsernameNotFoundException("User not found with name : " + name);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = facade.finByCode(id);
        if(user != null) return UserPrincipal.create(user);
        throw new UsernameNotFoundException("User not found with id : " + id);
    }
}
