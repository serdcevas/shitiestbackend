package com.example.GameServer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(" com.example.GameServer.mapper")
public class PersistenceConfig {



}
